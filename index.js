const ftp = require("basic-ftp")
const readXlsxFile = require('read-excel-file/node');


downloadFileFTP();

readFileAndValidate();

async function downloadFileFTP() {
    const client = new ftp.Client()
    client.ftp.verbose = true
    try {
        await client.access({
            host: "",
            user: "",
            password: "",
            secure: false
        })
        await client.downloadTo("/3it/newExcel.xlsx", "newExcel.xlsx")
    } catch (err) {
        console.log(err)
    }
    client.close();
}

async function readFileAndValidate() {
    const file = await readXlsxFile('C:\\3it\\newExcel.xlsx');

    file.forEach((row, i) => {
        if(i === 0) {
            return;
        }
        console.log(row);
    });
}